library ieee;
use ieee.std_logic_1164.all;

entity SegmentCounter is
	port(
		clk: in std_logic;
		reset: in std_logic;
		manual_input: in std_logic;
		input_out: out std_logic;
		reset_out: out std_logic;
		hex_0_0, hex_0_1, hex_0_2,hex_0_3,hex_0_4,hex_0_5, hex_0_6  : out std_logic;
		hex_1_0, hex_1_1, hex_1_2,hex_1_3,hex_1_4,hex_1_5, hex_1_6  : out std_logic;
		hex_2_0, hex_2_1, hex_2_2,hex_2_3,hex_2_4,hex_2_5, hex_2_6  : out std_logic
		);
end SegmentCounter;

architecture arch of SegmentCounter is
component GenericBinaryToBcdConverter is
	 generic( inputWidth: natural;
				digits: natural);
	 Port ( input : in  std_logic_vector (inputWidth - 1 downto 0);
           output : out  std_logic_vector (4 * digits - 1 downto 0));
end component;

component SevenSegmentDisplayDriver is
  port (
    clk        : in  std_logic;
    input : in  std_logic_vector(3 downto 0);
    o_Segment_0  : out std_logic;
    o_Segment_1  : out std_logic;
    o_Segment_2  : out std_logic;
    o_Segment_3  : out std_logic;
    o_Segment_4  : out std_logic;
    o_Segment_5  : out std_logic;
    o_Segment_6  : out std_logic
    );
	
end component;

component Counter is
	port( 
			clk: in std_logic;
			reset: in std_logic;
			O : out std_logic_vector(7 downto 0));
end component;

signal counterOut : std_logic_vector (7 downto 0);
signal tempOut: std_logic_vector(11 downto 0);
signal ones :  std_logic_vector (3 downto 0);
signal tens :  std_logic_vector (3 downto 0);
signal hundreds :  std_logic_vector (3 downto 0);

signal inverted_input, inverted_reset: std_logic;
begin
	inverted_input <= not manual_input;
	inverted_reset <= not reset;
	input_out <= inverted_input;
	reset_out <= inverted_reset;
	
	c: Counter
		port map(inverted_input, inverted_reset, counterOut);

	converter: GenericBinaryToBcdConverter
	 generic map (8, 3)
	 port map (counterOut, tempOut);
	ones <= tempOut(3 downto 0);
	tens <= tempOut(7 downto 4);
	hundreds <= tempOut(11 downto 8);
	
	ones_display: SevenSegmentDisplayDriver
	 port map (clk, ones, hex_0_0, hex_0_1, hex_0_2,hex_0_3,hex_0_4,hex_0_5, hex_0_6);
	 
	tens_display: SevenSegmentDisplayDriver
	 port map (clk, tens, hex_1_0, hex_1_1, hex_1_2,hex_1_3,hex_1_4,hex_1_5, hex_1_6 );
	 
	hundreds_display: SevenSegmentDisplayDriver
	port map (clk, hundreds, hex_2_0, hex_2_1, hex_2_2,hex_2_3,hex_2_4,hex_2_5, hex_2_6);
	
end arch;
