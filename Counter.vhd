library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;

entity Counter is
	port( 
			clk: in std_logic;
			reset: in std_logic;
			O : out std_logic_vector(7 downto 0));
end Counter;

architecture arch of Counter is
signal tOut : std_logic_vector(7 downto 0):= "00000000";
constant one : std_logic_vector(7 downto 0):= "00000001";
begin

 rec: process(reset, clk)
 begin
	if(reset = '1') then
			tOut <= "00000000";
	elsif(rising_edge(clk)) then
		tOut <= tOut + one;
	end if;
 end process;
 O <= tOut;
end arch;