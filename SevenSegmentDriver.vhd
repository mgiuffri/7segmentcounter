library ieee;
use ieee.std_logic_1164.all;
 
entity SevenSegmentDisplayDriver is
  port (
    clk        : in  std_logic;
    input : in  std_logic_vector(3 downto 0);
    o_Segment_0  : out std_logic;
    o_Segment_1  : out std_logic;
    o_Segment_2  : out std_logic;
    o_Segment_3  : out std_logic;
    o_Segment_4  : out std_logic;
    o_Segment_5  : out std_logic;
    o_Segment_6  : out std_logic
    );
end entity SevenSegmentDisplayDriver;
 
architecture arch of SevenSegmentDisplayDriver is
  signal convertedValue : std_logic_vector(6 downto 0) := (others => '0'); 
begin
  process (clk) is
  begin
    if rising_edge(clk) then
      case input is
        when "0000" =>
          convertedValue <= "0000001";
        when "0001" =>
          convertedValue <= "1001111";
        when "0010" =>
          convertedValue <= "0010010";
        when "0011" =>
          convertedValue <= "0000110";
        when "0100" =>
          convertedValue <= "1001100";          
        when "0101" =>
          convertedValue <= "0100100";
        when "0110" =>
          convertedValue <= "0100000";
        when "0111" =>
          convertedValue <= "0001111";
        when "1000" =>
          convertedValue <= "0000000";
        when "1001" =>
          convertedValue <= "0000100";
        when "1010" =>
          convertedValue <= "0001000";
        when "1011" =>
          convertedValue <= "1100000";
        when "1100" =>
          convertedValue <= "0110001";
        when "1101" =>
          convertedValue <= "1000010";
        when "1110" =>
          convertedValue <= "0010000";
        when "1111" =>
          convertedValue <= "0111000";
      end case;
    end if;
  end process;
  o_Segment_0 <= convertedValue(6);
  o_Segment_1 <= convertedValue(5);
  o_Segment_2 <= convertedValue(4);
  o_Segment_3 <= convertedValue(3);
  o_Segment_4 <= convertedValue(2);
  o_Segment_5 <= convertedValue(1);
  o_Segment_6 <= convertedValue(0);
end architecture arch;