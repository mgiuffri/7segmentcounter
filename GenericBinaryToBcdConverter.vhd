library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity GenericBinaryToBcdConverter is
	 generic( inputWidth: natural;
				digits: natural);
	 Port ( input : in  std_logic_vector (inputWidth - 1 downto 0);
           output : out  std_logic_vector ((4*digits - 1) downto 0));
end GenericBinaryToBcdConverter;

architecture Behavioral of GenericBinaryToBcdConverter is
begin

doubleDable: process(input)
  constant outputWidth : natural := 4 * digits;
  constant loopCount: natural := inputWidth - 4;
  variable temp : std_logic_vector (inputWidth - 1 downto 0);
  variable tempOutput : unsigned (outputWidth - 1 downto 0) := (others => '0');
  variable digitUpperIndex: natural := 0; 
  variable digitLowerIndex: natural := 0;
  begin
  
	 --initialise tempOutput and temp
    tempOutput := (others => '0');
	 tempOutput(2 downto 0) := unsigned(input(inputWidth - 1 downto inputWidth - 3));
	 temp := input(loopCount downto 0) & "000";

    for i in 0 to loopCount loop
		
		--Check Digit and Add 3 if more than 4
		for d in 1 to digits loop
			digitUpperIndex :=  (4 * d) - 1;
			digitLowerIndex :=  4 * (d - 1);
			if(tempOutput(digitUpperIndex downto digitLowerIndex) > 4) then
				tempOutput(digitUpperIndex downto digitLowerIndex) := tempOutput(digitUpperIndex downto digitLowerIndex) + 3;
			end if;	
		end loop;
		
		-- shift input to tempOutput (MSB goes to LSB)
      tempOutput := tempOutput((outputWidth - 2)  downto 0) & temp(inputWidth - 1);
		temp := temp((inputWidth - 2) downto 0) & '0';  
    end loop;
    output <= std_logic_vector(tempOutput);
  end process doubleDable;            
  
end Behavioral;